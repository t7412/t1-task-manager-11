package ru.t1.chubarov.tm.api;

public interface IProjectController {
    void createProject();

    void showProjects();

    void clearProjects();

    void showProjectByIndex();

    void showProjectById();

    void updateProjectByIndex();

    void updateProjectById();

    void removeProjectByIndex();

    void removeProjectById();
}
